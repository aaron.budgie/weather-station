# weather-station

raspberry pi weather station

## General configuration in Ubuntu 16.04 ##

1. Download [Etcher](https://etcher.io/) to flash the SD card.

2. Insert the SD card in the PC.

3. Execute Etcher. Once it is opened, select the RFID Raspbian Lite image, select the SD card as drive and click in "Flash!". It will take several minutes.

4. Plug SD Card in you WS Raspbian and boot up, first boot will take 1 or 2 minutes long.

5. In the boot folder create an empty file ssh

6. In the boot folder create a file wpa_supplicant.conf with this content

ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
country=ES
network={
	ssid="my ssid"
	scan_ssid=1
	psk="My PWD"
	key_mgmt=WPA-PSK
}

7. Git clone https://gitlab.com/aaron.budgie/weather-station-image.git.

8. execute ras-image-setup.sh

9. enable i2c pins using raspi-config command

10.  create logrotate.conf file:

### logrorate for putting the extension so the server can do the timelapse
/home/pi/img/img.jpg {
# We keep original file live
  copytruncate

# Rotation is 1 so we have always .1 as extension
  rotate 1

# If file is missing keep working
  missingok

  sharedscripts
  postrotate
    day=$(date +%Y-%h_%H:%M)
    mv /home/pi/img/img.jpg.1 /home/pi/img/img_$day.jpg
    find /home/pi/img -maxdepth 1 -mmin +59 -type f -delete
  endscript
}
####

11. create sh for camera with this content

fswebcam -r 1920x1080 -D 2 -S 20 --set brightness=30% --set constrast=0% -F 10 /home/pi/img/img.jpg

# that is for standard camera, not usb: raspistill -t 12000000 -tl 300000 -o /home/pi/img/img%04d.jpg

12. crontab -e

# crontab examples to run launcher and send camera images every 5 minutes
* * * * * python3 /home/pi/weather-station/launcher.py > /home/pi/backups/logs/weather.log 2> /home/pi/backups/logs/weather.err &
* * * * * /home/pi/camera.sh > /home/pi/backups/logs/camera.log 2> /home/pi/backups/logs/camera.err &
* * * * * rsync -a --ignore-existing --exclude 'output.gif' /home/pi/img/ root@xx.xx.xx.xx:/opt/ah-addons/weather_website/static/src/img/ --delete > /home/pi/backups/logs/scp.log 2> /home/pi/backups/logs/scp.err
# crontab for low battery safe shutdown
* * * * * python3 /home/pi/weather-station/LowBatteryDetector.py > /home/pi/backups/logs/battery.log 2> /home/pi/backups/logs/battery.err &
* * * * * /usr/sbin/logrotate -f /home/pi/logrotate.conf > /home/pi/backups/logs/rotate.log 2> /home/pi/backups/logs/rotate.err &

Additional info
===============

# command to create timelapse from jpg (this goes in Odoo side)
call sh with this content
cd /opt/ah-addons/weather_website/static/src/img/
ffmpeg -framerate 1 -pattern_type glob -i '*.jpg' -c:v libx264 -r 30 -pix_fmt yuv420p out.mp4 -y
ffmpeg -i out.mp4 -vf "fps=10,scale=640:-1:flags=lanczos,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse" -loop 0 output.gif -y
