from selenium import webdriver
import time
import os
import json
WORK_DIR = '/home/odoo/weather-station'
if os.path.isfile(os.path.abspath(
        os.path.join(WORK_DIR, 'dicts/data.json'))):
    json_file = open(os.path.abspath(
        os.path.join(WORK_DIR, 'dicts/data.json')))
    json_data = json.load(json_file)
    json_file.close()
    
    url = json_data["hotspot_url"]
    hotspot_pwd = json_data["hotspot_pwd"]

    options = webdriver.ChromeOptions()
    options.add_argument("headless")
    driver = webdriver.Chrome(chrome_options=options)
    driver.get(url)
    time.sleep(20)
    form_box = "//input[@class='form-control']"
    conditions = "//ins[@class='iCheck-helper']"
    button_logging = "//input[@class='btn btn-primary btn-block btn-flat']"

    #put code
    driver.find_element_by_xpath(form_box).send_keys(hotspot_pwd)
    #accept conditions
    driver.find_element_by_xpath(conditions).click()
    driver.find_element_by_xpath(button_logging).click()
