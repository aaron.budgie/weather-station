# safe shutdown when low battery
import os
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
shutdown_pin = 17  # defines pin 17 as the pin we're watching
GPIO.setup(shutdown_pin, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
def shutdown_callback_function( shutdown_pin ):
    print("the high battery pin is LOW now, shutting down.")
    os.system("sudo shutdown -r now")
# This is the magic line that adds pin 17 so it is always being watched.
GPIO.add_event_detect(shutdown_pin, GPIO.FALLING, callback=shutdown_callback_function, bouncetime=250)

