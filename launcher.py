import json
import logging
import os
import threading
import time
import subprocess
import RPi.GPIO as GPIO

from lib.bme280_sensor import BME280
from lib import odoo_xmlrpc, display_drawing
from lib.reset_lib import is_wifi_active

# OLED = display_drawing.DisplayDrawing()
# todo uncomment this

_logger = logging.getLogger(__name__)

WORK_DIR = '/home/pi/weather-station/'

card_found = False

turn_off = False
on_menu = True
pos = 0
enter = False
on_Down = False
on_OK = False
ap_mode = False

odoo = False


GPIO.setmode(GPIO.BOARD)  # Set's GPIO pins to BCM GPIO numbering

def ws_reader(station_id=1):
    # todo fetch station
    data = BME280.read()
    if data:
        _logger.info("%s HS read", data)
        print(data)
        station_id = odoo.odoo.env['weather.station'].get_weather_station_id()
        print(station_id)
        odoo.odoo.env['weather.data'].register_weather_station_data(station_id, data)
        # OLED._display_data(data)
        time.sleep(3)
    return data


def instance_connection():
    global admin_id
    if os.path.isfile(os.path.abspath(
            os.path.join(WORK_DIR, 'dicts/data.json'))):
        json_file = open(os.path.abspath(
            os.path.join(WORK_DIR, 'dicts/data.json')))
        json_data = json.load(json_file)
        json_file.close()
        host = json_data["odoo_host"][0]
        port = json_data["odoo_port"][0]
        user_name = json_data["user_name"][0]
        user_password = json_data["user_password"][0]
        dbname = json_data["db"][0]
        os.environ['TZ'] = "Europe/Madrid"
        time.tzset()
        _logger.info('Instancing OdooXmlRPC')
        odoo = odoo_xmlrpc.OdooXmlRPC(host, port, dbname, user_name,
                                      user_password)
        return odoo
    else:
        _logger.info('data.json not found')
        return False


def run():
    global enter, turn_off
    global on_menu
    global odoo
    if True:
        print('met data')
        while not odoo:
            _logger.info("No Odoo connection available")
            odoo = instance_connection()
            if not odoo:
                time.sleep(3)
        else:
            res = ws_reader()  # read_data
    else:
        # no connection, wait and try again
        time.sleep(60)
        run()


if __name__ == '__main__':
    try:
        run()
    except KeyboardInterrupt:
        _logger.info("KeyboardInterrupt")
        GPIO.cleanup()
