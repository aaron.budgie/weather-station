import logging
import xmlrpc.client as xmlrpclib
import odoorpc

_logger = logging.getLogger(__name__)


class OdooXmlRPC(object):
    
    def __init__(self, host, port, db, user, pwd):
        odoo = odoorpc.ODOO(host=host, port=port)
        odoo.login(db, user, pwd)
        self.odoo = odoo

    def get_weather_station_id(self):
        try:
            res = self.odoo.env['weather.station'].get_weather_station_id()
            _logger.debug(res)
            return res
        except Exception as e:
            _logger.debug("get_weather_station exception request: " + str(e))
            return False

    def register_weather_station_data(self, station_id, data):
        try:
            res = self.odoo.env['weather.data'].register_weather_station_data()
            _logger.debug(res)
            return res
        except Exception as e:
            _logger.debug("get_weather_station exception request: " + str(e))
            return False
