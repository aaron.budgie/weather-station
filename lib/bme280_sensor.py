
# sudo apt-get install -y python-smbus
# sudo apt-get install -y i2c-tools
import smbus2
import bme280
port = 1
address = 0x76
bus = smbus2.SMBus(port)

class BME280:

    def read():
        with open("temp.txt", "w") as fh:
            calibration_params = bme280.load_calibration_params(bus, address)
            data = bme280.sample(bus, address, calibration_params)
            temperature = data.temperature
            humidity = data.humidity
            pressure = data.pressure
        return {
            'temperature': temperature,
            'humidity': humidity,
            'pressure': pressure
        }
